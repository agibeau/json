package com.example.app;

public class Animal
{
    protected String type;
    protected String name;
    protected String pname;
    protected String desc;
    protected String ls;
    protected String weight;
    protected String age;
    protected String url;

	public Animal ()
	{
	    type = name = pname = desc = ls = weight = age = url = "";
	    	}
	
	public Animal (Animal A)
	{
		type = A.type;
		name = A.name;
		pname = A.pname;
		desc = A.desc;
		ls = A.ls;
		weight = A.weight;
		age = A.age;
        url = A.url;
	}

    public Animal (String S){
        name = S;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String T)
    {
        name = T;
    }

    public String getType ()
    {
	return type;
    }

    public void setType (String T)
    {
	type = T;
    }

    public String getPname ()
    {
        return pname;
    }

    public void setPname (String T)
    {
        pname = T;
    }

    public String getDesc ()
    {
        return desc;
    }

    public void setDesc (String T)
    {
        desc = T;
    }

    public String getLs ()
    {
        return ls;
    }

    public void setLs (String T)
    {
        ls = T;
    }

    public String getWeight ()
    {
        return weight;
    }

    public void setWeight (String T)
    {
        weight = T;
    }

    public String getAge ()
    {
        return age;
    }

    public void setAge (String T)
    {
        age = T;
    }

    public String getCon () {return "Placeholder";}

    public String getWs () {return "Placeholder";}

    public String getHab () {return "Placeholder";}

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String T)
    {
        url = T;
    }

}
