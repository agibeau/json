package com.example.app;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;

import com.loopj.android.http.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements listViewFragment.OnDetailView {
    List<Animal> animalCategories;
    List<Animal> aquaticAnimals;
    List<Animal> flyingAnimals;
    List<Animal> landAnimals;
    listViewFragment animalListV;
    listViewFragment aquaticListV;
    listViewFragment flyingListV;
    listViewFragment landListV;
    DetailsFragment detailsFragment;
    DescriptionFragment descriptionFragment;

    JSONArray jArray;
    public String json = "https://bitbucket.org/agibeau/project3/raw/5052902c0a37d3fedd0a1ca87920c9a24b8233a0/Animals.json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        animalCategories = new ArrayList<Animal>();
        animalCategories.add(new Animal("Land"));
        animalCategories.add(new Animal("Aquatic"));
        animalCategories.add(new Animal("Flying"));

        aquaticAnimals = new ArrayList<Animal>(); flyingAnimals = new ArrayList<Animal>(); landAnimals = new ArrayList<Animal>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(json, new AsyncHttpResponseHandler() {

            // When the JSON has been fetched, we will process it
            // The JSON is simply a string at this point. Now because the JSON is formated as a
            // JSON Array, we will parse it as an Array, and then loop over each JSON Object
            // Fetch that object, and parse out two values from it, and put them into our
            // Simple Object Class.
            @Override
            public void onSuccess(String response) {

                try {
                    int count = 0;
                    jArray = new JSONArray(response); // Parse JSON String to JSON Array
                    //int count = 0;
                    for (int i = 0; i < jArray.length(); ++i) { // Loop over Array
                        //SimpleObject simpleObject = new SimpleObject(); // Create a new Simple Object
                        String obType;
                        JSONObject jObject = jArray.getJSONObject(i); // Fetch the ith JSON Object
                        //from the JSON Array

                        if (jObject.getString("Type").equals("Land")) {
                            Land ani = new Land();
                            ani.setType(jObject.getString("Type"));
                            ani.setName(jObject.getString("Name"));
                            ani.setDesc(jObject.getString("Description"));
                            ani.setCon(jObject.getString("Continent"));
                            ani.setLs(jObject.getString("Lifespan"));
                            ani.setPname(jObject.getString("Petname"));
                            ani.setAge(jObject.getString("Age"));
                            ani.setWeight(jObject.getString("Weight"));
                            ani.setUrl(jObject.getString("URL"));
                            landAnimals.add(ani);
                        }

                        if (jObject.getString("Type").equals("Aquatic")) {
                            Aquatic ani = new Aquatic();
                            ani.setType(jObject.getString("Type"));
                            ani.setHab(jObject.getString("Habitat"));
                            ani.setDesc(jObject.getString("Description"));
                            ani.setName(jObject.getString("Name"));
                            ani.setLs(jObject.getString("Lifespan"));
                            ani.setPname(jObject.getString("Petname"));
                            ani.setAge(jObject.getString("Age"));
                            ani.setWeight(jObject.getString("Weight"));
                            ani.setUrl(jObject.getString("URL"));
                            aquaticAnimals.add(ani);
                        }

                        if (jObject.getString("Type").equals("Flying")){
                            Flying ani = new Flying();
                            ani.setType(jObject.getString("Type"));
                            ani.setCon(jObject.getString("Continent"));
                            ani.setDesc(jObject.getString("Description"));
                            ani.setName(jObject.getString("Name"));
                            ani.setLs(jObject.getString("Lifespan"));
                            ani.setWs(jObject.getString("Wingspan"));
                            ani.setPname(jObject.getString("Petname"));
                            ani.setAge(jObject.getString("Age"));
                            ani.setWeight(jObject.getString("Weight"));
                            ani.setUrl(jObject.getString("URL"));
                            flyingAnimals.add(ani);
                        }

                        count++;
                    }
                    Log.d("List Size", "Size of items: " + Integer.toString(count));
                } catch (JSONException e) {
                    Log.d("JSON Parse", e.toString());
                }
            }
        });

        /*Land parrot = new Land();
        //parrot.setWs("5-10");
        parrot.setType("Flying");
        parrot.setLs("30 years");
        parrot.setName("Parrot");
        landAnimals.add(parrot);/*

        /*flyingAnimals.add(); flyingAnimals.add(new Animal("Sparrow"));
        aquaticAnimals.add(new Animal("Penguin")); aquaticAnimals.add(new Animal("Whale"));
        landAnimals.add(new Animal("lion")); landAnimals.add(new Animal("Bear"));*/

        animalListV = new listViewFragment(animalCategories, "Animals");

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.listViewContainer, animalListV)
                    .add(R.id.detailsContainer, new DetailsFragment("San Francisco Zoo", "1 Zoo Rd, San Francisco, CA 94132"))
                    .commit();

        }
        AsyncHttpClient client1 = new AsyncHttpClient();
        String[] allowedContentTypes = new String[] { "image/png", "image/jpeg" };
        client1.get("http://snowleopardconservancy.org/rofl/wp-content/uploads/2012/12/sfzoo_logo_facebook.jpg", new BinaryHttpResponseHandler(allowedContentTypes) {
            @Override
            public void onSuccess(byte[] fileData) {
                Bitmap imageBitmap = BitmapFactory.decodeByteArray(fileData, 0, fileData.length);
                if (detailsFragment != null)
                    detailsFragment.setImageView(imageBitmap);
            }
        });
        detailsFragment = new DetailsFragment("San Francisco Zoo", "1 Zoo Rd, San Francisco, CA 94132");

        getFragmentManager().beginTransaction()
                .replace(R.id.detailsContainer, detailsFragment)
                .commit();

    }

    @Override
    public void onDetailViewUpdate(int position, String name, List<Animal> item) {
        if (name.equals("Animals")){
            detailsFragment = new DetailsFragment(item.get(position).getName(), item.get(position).getPname());
            if (animalCategories.get(position).getName().equals("Aquatic")){
                aquaticListV = new listViewFragment(aquaticAnimals, "Aquatic");
                getFragmentManager().beginTransaction()
                    .replace(R.id.listViewContainer, aquaticListV)
                    .replace(R.id.detailsContainer, detailsFragment)
                    .addToBackStack("DetailBack")
                    .commit();
            }
            else if (animalCategories.get(position).getName().equals("Flying")){
                flyingListV = new listViewFragment(flyingAnimals, "Flying");
                getFragmentManager().beginTransaction()
                    .replace(R.id.listViewContainer, flyingListV)
                    .replace(R.id.detailsContainer, detailsFragment)
                     .addToBackStack("DetailBack")
                    .commit();
            }
            else if (animalCategories.get(position).getName().equals("Land")){
                landListV = new listViewFragment(landAnimals, "Land");
                getFragmentManager().beginTransaction()
                        .replace(R.id.listViewContainer, landListV)
                        .replace(R.id.detailsContainer, detailsFragment)
                        .addToBackStack("DetailBack")
                        .commit();

            }
        }
        else{
            AsyncHttpClient client = new AsyncHttpClient();
            String[] allowedContentTypes = new String[] { "image/png", "image/jpeg" };
            client.get(item.get(position).getUrl(), new BinaryHttpResponseHandler(allowedContentTypes) {
                @Override
                public void onSuccess(byte[] fileData) {
                    Bitmap imageBitmap = BitmapFactory.decodeByteArray(fileData, 0, fileData.length);
                    if (detailsFragment != null)
                        detailsFragment.setImageView(imageBitmap);
                }
            });
            detailsFragment = new DetailsFragment(item.get(position).getName(), item.get(position).getPname());
            descriptionFragment = new DescriptionFragment(item.get(position));
            getFragmentManager().beginTransaction()
                    .replace(R.id.detailsContainer, detailsFragment)
                    .replace(R.id.listViewContainer, descriptionFragment)
                    .addToBackStack("DetailBack")
                    .commit();

        }
    }
}
