package com.example.app;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 11/20/13.
 */
public class listViewFragment extends Fragment implements AdapterView.OnItemClickListener {
    List<Animal> animals;
    List<String> items;
    ListView listView;
    String listName;
    ArrayAdapter<String> adapter;
    Context context;
    OnDetailView mListener;

    public listViewFragment( List<Animal> animals, String name) {
        this.listName = name;
        this.animals = animals;
        this.items = convertAniToString(animals);
    }

    // Makes our context for us
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = activity;
        if (activity instanceof OnDetailView){
            mListener = (OnDetailView) activity;
        }
     else {
        throw new ClassCastException(activity.toString() + "is lame" );
    }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_view_fragment_layout, container, false);

        // References our xml file, giving us access to the listView
        listView = (ListView) view.findViewById(R.id.listView);
        adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, items);
        listView.setAdapter(adapter);
        // Tell our list view about the click listener
        listView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        mListener.onDetailViewUpdate(position, listName, animals);
    }

    public interface OnDetailView {
        public void onDetailViewUpdate(int position, String listName, List<Animal> items);
    }

    private List<String> convertAniToString (List<Animal> anis){
        List<String> strList = new ArrayList<String>();
        for (int i=0;i<anis.size();i++)
            strList.add(anis.get(i).getName());
        return strList;
    }
}